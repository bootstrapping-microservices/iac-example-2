#
# Destroys a microservice deployed to Kubernetes.
#

set -u # or set -o nounset
: "$SERVICE_NAME"
: "$VERSION"
: "$KUBE_HOST"
: "$KUBE_CLIENT_CERTIFICATE"
: "$KUBE_CLIENT_KEY"
: "$KUBE_CLUSTER_CA_CERTIFICATE"
: "$DOCKER_REGISTRY"

cd scripts
terraform apply -auto-approve \
    -var "service_name=$SERVICE_NAME" \
    -var "service_version=$VERSION" \
    -var "kube_host=$KUBE_HOST" \
    -var "kube_client_certificate=$KUBE_CLIENT_CERTIFICATE" \
    -var "kube_client_key=$KUBE_CLIENT_KEY" \
    -var "kube_cluster_ca_certificate=$KUBE_CLUSTER_CA_CERTIFICATE" \
    -var "docker_registry=$DOCKER_REGISTRY"
cd ..