# Deploys the microservice to the Kubernetes cluster.

resource "kubernetes_deployment" "service_deployment" {

    metadata {
        name = var.service_name

    labels = {
            pod = var.service_name
        }
    }

    spec {
        replicas = 1

        selector {
            match_labels = {
                pod = var.service_name
            }
        }

        template {
            metadata {
                labels = {
                    pod = var.service_name
                }
            }

            spec {
                container {
                    image = "${var.docker_registry}/${var.service_name}:${var.service_version}"
                    name  = var.service_name

                    env {
                        name = "PORT"
                        value = "80"
                    }

                    # Configure other environment variables here.
                }
            }
        }
    }
}

resource "kubernetes_service" "service" {
    metadata {
        name = var.service_name
    }

    spec {
        selector = {
            pod = kubernetes_deployment.service_deployment.metadata[0].labels.pod
        }   

        session_affinity = "ClientIP"

        port {
            port        = 80
            target_port = 80
        }

        type             = "LoadBalancer"
    }
}
