#
# Build a Docker image.
#
# Environment variables:
#
#   SERVICE_NAME - The name of the service in this repository.
#   DOCKER_REGISTRY - The hostname of your private Docker registry.
#   DOCKER_UN - User name for your Docker registry.
#   DOCKER_PW - Password for your Docker registry.
#   VERSION - The version number to tag the images with.
#
# Usage:
#
#       ./scripts/push-image.sh
#

set -u # or set -o nounset
: "$SERVICE_NAME"
: "$DOCKER_REGISTRY"
: "$VERSION"
: "$DOCKER_UN"
: "$DOCKER_PW"

docker login $DOCKER_REGISTRY --username $DOCKER_UN --password $DOCKER_PW
docker push $DOCKER_REGISTRY/$SERVICE_NAME:$VERSION
