# Sets global variables for this Terraform project.

variable service_name {
}

variable service_version {
}

variable kube_host {
}

variable kube_client_certificate {
}

variable kube_client_key {
}

variable kube_cluster_ca_certificate {   
}

variable docker_registry {
}