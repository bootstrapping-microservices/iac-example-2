#
# Build a Docker image.
#
# Environment variables:
#
#   SERVICE_NAME - The name of the service in this repository.
#   DOCKER_REGISTRY - The hostname of your private Docker registry.
#   VERSION - The version number to tag the images with.
#
# Usage:
#
#       ./scripts/build-image.sh
#

set -u # or set -o nounset
: "$SERVICE_NAME"
: "$DOCKER_REGISTRY"
: "$VERSION"

docker build -t $DOCKER_REGISTRY/$SERVICE_NAME:$VERSION --file ./Dockerfile-prod .