#
# Deploys a microservice to a Kubernetes cluster.
#

set -e # Any subsequent commands that fail exit the script with an error.
set -u # or set -o nounset
: "$SERVICE_NAME"
: "$VERSION"
: "$TFSTATE_BACKEND_RES_GROUP"
: "$TFSTATE_BACKEND_STORAGE_ACC"
: "$TFSTATE_BACKEND_CONTAINER"
: "$TFSTATE_BACKEND_KEY"
: "$KUBE_HOST"
: "$KUBE_CLIENT_CERTIFICATE"
: "$KUBE_CLIENT_KEY"
: "$KUBE_CLUSTER_CA_CERTIFICATE"
: "$DOCKER_REGISTRY"

cd ./scripts
terraform init \
    -backend-config="resource_group_name=$TFSTATE_BACKEND_RES_GROUP" \
    -backend-config="storage_account_name=$TFSTATE_BACKEND_STORAGE_ACC" \
    -backend-config="container_name=$TFSTATE_BACKEND_CONTAINER" \
    -backend-config="key=$TFSTATE_BACKEND_KEY"
terraform apply -auto-approve \
    -var "service_name=$SERVICE_NAME" \
    -var "service_version=$VERSION" \
    -var "kube_host=$KUBE_HOST" \
    -var "kube_client_certificate=$KUBE_CLIENT_CERTIFICATE" \
    -var "kube_client_key=$KUBE_CLIENT_KEY" \
    -var "kube_cluster_ca_certificate=$KUBE_CLUSTER_CA_CERTIFICATE" \
    -var "docker_registry=$DOCKER_REGISTRY"
cd ..