# Initialises Terraform providers and sets their version numbers.

provider "kubernetes" {
    version = "1.12.0"

    load_config_file = "false"
    
    host = var.kube_host

    client_certificate        = base64decode(var.kube_client_certificate)
    client_key                = base64decode(var.kube_client_key)
    cluster_ca_certificate    = base64decode(var.kube_cluster_ca_certificate)
}
